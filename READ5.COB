       IDENTIFICATION DIVISION.
       PROGRAM-ID. READ5.
       AUTHOR. WARITPHAT.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT 100-INPUT-FILE ASSIGN TO "MEMBER.DAT" 
              ORGANIZATION IS SEQUENTIAL
              FILE STATUS IS WS-INPUT-FILE-STATUS.

       DATA DIVISION.
       FILE SECTION.
       FD  100-INPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01  100-INPUT-RECORD.
           05 MEMBER-ID         PIC X(5).
           05 MEMBER-NAME       PIC X(20).
           05 MEMBER-SELECTED   PIC 9.
           05 MEMBER-GENDER     PIC X.
           05 FILLER            PIC X(53).

       WORKING-STORAGE SECTION. 
       01  WS-INPUT-FILE-STATUS PIC   X(2).
           88 FILE-IS-OK              VALUE    '00'.
           88 FILE-AT-END             VALUE    '10'.

       01  WS-INPUT-COUNT       PIC      9(7)  VALUE ZEROS.
       01  WS-FEMALE            PIC      9(7)  VALUE ZEROES.
       01  WS-MALE              PIC      9(7)  VALUE ZEROES.
       01  WS-CHOICE-01         PIC      9(7)  VALUE ZEROES.
       01  WS-CHOICE-02         PIC      9(7)  VALUE ZEROES.

       PROCEDURE DIVISION.
      *MAIN PROGRAM 
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAL THRU 1000-EXIT
           PERFORM 2000-PROCESS THRU 2000-EXIT UNTIL FILE-AT-END
           PERFORM 3000-END THRU 3000-EXIT
           GOBACK.

      *INITIAL SECTION
       1000-INITIAL.
           OPEN INPUT 100-INPUT-FILE.
           IF NOT FILE-IS-OK
              DISPLAY "File not found!!!"
              STOP RUN
           END-IF
           PERFORM 8000-READ THRU 8000-EXIT.
       1000-EXIT.
           EXIT.
      *PROCESS SECTION
       2000-PROCESS.
           DISPLAY WS-INPUT-FILE-STATUS
           DISPLAY 100-INPUT-RECORD
           ADD 1 TO WS-INPUT-COUNT
           PERFORM 2001-COUNT-GENDER THRU 2001-EXIT.
           PERFORM 2002-COUNT-SELECTED THRU 2002-EXIT.
           PERFORM 8000-READ THRU 8000-EXIT.
       2000-EXIT.
           EXIT.
      *COUNT GENDER    
       2001-COUNT-GENDER.
           IF MEMBER-GENDER = "F"
              ADD 1 TO WS-FEMALE
           ELSE
              ADD 1 TO WS-MALE
           END-IF.
       2001-EXIT.
           EXIT. 
      *COUNT CHOICE
       2002-COUNT-SELECTED.
           IF MEMBER-SELECTED = "0"
              ADD 1 TO WS-CHOICE-01
           ELSE 
              ADD 1 TO WS-CHOICE-02
           END-IF.
       2002-EXIT.
           EXIT. 
      *END SECTION
       3000-END.
           CLOSE 100-INPUT-FILE.
           DISPLAY WS-INPUT-FILE-STATUS.
           DISPLAY "READ: " WS-INPUT-COUNT.
           DISPLAY "FEMALE: " WS-FEMALE.
           DISPLAY "MALE: " WS-MALE.
           DISPLAY "CHOICE 01: " WS-CHOICE-01.
           DISPLAY "CHOICE 02: " WS-CHOICE-02.
       3000-EXIT.
           EXIT.
      *READ SECTION
       8000-READ.
           READ 100-INPUT-FILE.
       8000-EXIT.
           EXIT.